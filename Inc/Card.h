#include "Enums.h"
class Card
{
  private:
    //member variables
    CardType m_type; //the type of card, always valid
    unsigned int m_value; //int representation of card value

    //private functions
    void init(CardType type, unsigned int value);
  public:
    //constructors
    Card();
    Card(CardType type, unsigned int value = 1);

    //getters
    CardType getType() const;
    unsigned int getValue() const;
};
