enum class CardTypes
{
  Suspect,
  Weapon,
  Room
};

enum class Suspects : unsigned int
{
  ProfPlum = 1,
  MrsWhite,
  MrsScarlet,
  MrGreen,
  MrsPeacock,
  ColMustard,
  None
};

enum class Weapons : unsigned int
{
  Knife = 1,
  Candlestick,
  Rope,
  Revolver,
  LeadPipe,
  Wrench,
  None
};

enum class Rooms : unsigned int
{
  Kitchen = 1,
  Ballroom,
  Conservatory,
  BilliardRoom,
  Library,
  Study,
  Hall,
  Lounge,
  DiningRoom,
  None
};
